#!/bin/bash

readonly logFmt="(${0##*/}:%d): %s\\n"

##### DEFAULTS
optTarget='tmp'
optDirs=96
optFiles=$(( optDirs ** 2 ))
optLength=32
optSeed=1280202010

##### HELPERS

type log > /dev/null 2>&1 || log() {
	printf -- "$logFmt" "${BASH_LINENO[0]}" "$*"
} >&2

type log > /dev/null 2>&1 || fatal() {
	[ $# -eq 0 ] || log "$@"
	exit 1
}

usage() {
	cat <<- USAGE
		Creates a bunch of test files.
		Usage: ${0##*/} [option]...

		OPTIONS:
		--force
		        Forces removal of existing target
		-t, --target DIR  
		        Generates files in given target directory
		-d, --dirs NUM
		        Generates NUM directories
		-f, --files NUM
		        Generates NUM files
		-l, --length NUM
		        Generated names are at most NUM characters long
		-s, --seed NUM
		        Random number generator is seeded with NUM
		-h, --help
		        Displays this help message
	USAGE

	[ $# -eq 0 ] || fatal "$@"
	exit 0
} >&2

assertNumber() {
	[ $# -eq 2 ] || fatal 'assertNumber needs 2 args'
	printf '%s' "$1" | grep -Eq '^[[:digit:]]+$' \
		|| usage "$2 must be a number, not '$1'"
}

assertNumberPositive() {
	assertNumber "$1" "$2"
	[ "$1" -gt 0 ] \
		|| usage "$2 must be > 0, not '$1'"
}

parseArgs() {
	allArgs="$@"
	options="$(getopt \
		-o 't:d:f:l:s:h' -l 'force,target:,dirs:,files:,length:,seed:,help' \
		-n "${0##*/}" -- "$@")"
	[ $? != 0 ] && usage
	eval set -- "$options"
	unset options
	while : ; do
		case "$1" in
			--force)
				optForce='t'
				shift ; continue # no arg
				;;
			-t|--target)
				optTarget="$2"
				;;
			-d|--dirs)
				optDirs="$2"
				;;
			-f|--files)
				optFiles="$2"
				;;
			-l|--length)
				optLength="$2"
				;;
			-s|--seed)
				optSeed="$2"
				;;
			-h|--help)
				usage
				;;
			--)
				shift; break ;;
			*)
				usage "Invalid arguments: '$1'" ;;
		esac
		shift 2
	done
	[ $# -eq 0 ] || usage "Illegal arguments: $*"

	readonly optTarget optDirs optFiles optLength optSeed
	log "${0##*/} -t '$optTarget' -d $optDirs -f $optFiles -l $optLength -s $optSeed"

	[ -n "$optTarget" ] || usage "--target must not be empty"
	assertNumberPositive "$optDirs" "--dirs"
	assertNumberPositive "$optFiles" "--files"
	assertNumberPositive "$optLength" "--length"
	assertNumber "$optSeed" "--seed"
	[ $optLength -le 255 ] || usage "--length must be <= 255"
}

parseArgs "$@"

##### program

# optTarget
if [ -e "$optTarget" ] ; then
	[ -n "$optForce" ] || while : ; do
		read -p "'$optTarget' exists. Delete? [yN] "
		case "$REPLY" in
			''|[Nn]|[Nn][Oo])  exit 1 ;;
			[Yy]|[Yy][Ee][Ss]) break ;;
			*)                 log "Invalid selection: '$REPLY'" ;;
		esac
	done
	log "Removing: '$optTarget'"
	rm -vr "$optTarget" | tail -1
	[ ! -e "$optTarget" ] || fatal "'$optTarget' still exists" "Cannot proceed"
fi

mkdir -pv "$optTarget" \
&& cd "$optTarget" \
|| fatal

( ##### RANDOM CHARACTER GENERATOR
	# makes a stream of random characters
	RANDOM=$(( optSeed + 1 ))

	chars[0]='abcdefghijklmnopqrstuvwxyz'
	chars[1]="$( tr '[:lower:]' '[:upper:]' <<< ${chars[0]} )"
	chars[2]="0123456789"
	chars[3]='.'
	chars[4]=' '
	chars[5]='@#$%^&()_+=~`[]{};,"'

	prob[0]=200
	prob[1]=30
	prob[2]=20
	prob[3]=10
	prob[4]=5
	prob[5]=1

	eval $( tr ' ' '\n' <<< "${prob[*]}" \
		| awk '{ while ( $1-- ) print NR-1 }' \
		| awk '{ print "prob[" NR-1 "]=" $1 }' )
	probMod=${#prob[*]}

	while : ; do
		r=$((RANDOM % probMod))
		charSet=${chars[${prob[$r]}]}
		r=$((RANDOM % ${#charSet}))
		printf '%c' "${charSet:$r:1}"
	done
) | (

# eats a stream of random characters

RANDOM=$(( optSeed + 2 ))

getRandDir() {
	local i=$(( RANDOM * RANDOM % ${#dirs[@]} ))
	eval "$1"="'${dirs[$i]}'"
}

progress() {
	printf '\r%5s: %i / %-i (%02i%%) ' "$1" $2 $3 $(( 100 * $2 / $3 ))
} >&2

getRandChars() {
	local len=$(( RANDOM % optLength + 1 ))
	read -rN $len REPLY
	eval "$1"="'$REPLY'"
}

makeDirs() {
	dirs[0]='.'
	local dir
	local chars
	for i in $( seq $1 ) ; do
		getRandDir dir
		getRandChars chars
		mkdir -p "$dir/$chars"
		readarray -t dirs <<< "$(find -type d -print | sort)"
		progress "Dir" $i $1
	done
	declare -ar dirs
	echo >&2
}

makeFiles() {
	local dir
	local chars
	for i in $( seq $1 ) ; do
		getRandDir dir
		getRandChars chars
		touch "$dir/$chars"
		progress "File" $i $1
	done
	readarray -t files <<< "$(find -type f -print | sort)"
	declare -ar files
	echo >&2
}

getRandFile() {
	local i=$(( RANDOM * RANDOM % ${#files[@]} ))
	eval "$1"="'${files[$i]}'"
}

sizeFiles() {
	local num=$1
	local min=$2
	local max=$3
	local range=$(( max - min ))
	local size
	local file
	for i in $( seq $num ); do
		getRandFile file
		size=$(( RANDOM * RANDOM % range + min ))
		cat /dev/zero | head -c $size > "$file"
	done
}

dateFiles() {
	local num=$1
	local date="$2"
	local file
	for i in $( seq $num ); do
		getRandFile file
		touch -d "$date" "$file"
	done
}

SECONDS=0

makeDirs $optDirs
makeFiles $optFiles
sizeFiles 10 280 510
sizeFiles 10 1060 4090
dateFiles 10 "7 months ago"
dateFiles 10 "14 months ago"
dateFiles 10 "last week"
dateFiles 10 "tomorrow"

[ "$SECONDS" != '0' ] \
	&& log "Finised in ${SECONDS}s"

find -print0 | sort -z | xargs -0 stat --format='%s %n' | shasum

)
